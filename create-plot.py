import matplotlib
import matplotlib.pyplot as plt
import csv
import tikzplotlib as tikzp

matplotlib.use("pgf")
plt.style.use("seaborn-bright")
plt.rc("text", usetex=True)

x = []
y = []

with open("example.csv") as csvfile:
    plots = csv.reader(csvfile, delimiter=";")
    for row in plots:
        x.append(int(row[0]))
        y.append(int(row[1]))

plt.plot(x, y)
plt.xlabel("x")
plt.ylabel("y")
tikzp.save("plot.pgf", axis_height = "\\figureheight", axis_width = "\\figurewidth")
